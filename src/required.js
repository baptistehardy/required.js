class Required {
    inputs = document.getElementsByTagName("input")
    labels = document.getElementsByTagName("label")

    constructor(params) {
        this.setRequired(params)
    }

    getRequired = (color = "red", content = "*") => {
        let required = document.createElement("span");
        required.innerHTML = content
        required.style.color = color
        return required
    }

    setRequired = (params) => {
        Array.from(this.inputs).forEach((input) => {
            if(input.hasAttribute("required")) {
                Array.from(this.labels).forEach((label) => {
                    if(label.hasAttribute("for")) {
                        if(label.getAttribute("for") === input.getAttribute("id")) {
                            label.appendChild(this.getRequired(params['color'], params['content']))
                        }
                    }
                });
            }
        });
    }
}