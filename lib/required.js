"use strict";

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var Required = function Required(_params) {
  var _this = this;

  _classCallCheck(this, Required);

  _defineProperty(this, "inputs", document.getElementsByTagName("input"));

  _defineProperty(this, "labels", document.getElementsByTagName("label"));

  _defineProperty(this, "getRequired", function () {
    var color = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "red";
    var content = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "*";
    var required = document.createElement("span");
    required.innerHTML = content;
    required.style.color = color;
    return required;
  });

  _defineProperty(this, "setRequired", function (params) {
    Array.from(_this.inputs).forEach(function (input) {
      if (input.hasAttribute("required")) {
        Array.from(_this.labels).forEach(function (label) {
          if (label.hasAttribute("for")) {
            if (label.getAttribute("for") === input.getAttribute("id")) {
              label.appendChild(_this.getRequired(params['color'], params['content']));
            }
          }
        });
      }
    });
  });

  this.setRequired(_params);
};