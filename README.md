# Required.js

Very simple & lightweight JS library to automatically add content next to labels of *required* inputs.

### Usage

##### Setup

Load *required.js*.

```html
<script rel="script" src="path/to/required.js"></script>
<script rel="script">
    new Required(options);
</script>
```

##### Condition

It'll *only* work on labels referring to an input with a `for` attribute.

```html
<label for="arequiredinput">Label</label>
<input type="text" id="arequiredinput" required>
```


#### Options

Key | Option type | Example
----|---------|------
`content` | string | `"(required)"`
`color` | HEX (string) <br/> color name (string) | `"#ff0000"` <br/> `"red"`

##### Example

The following values correspond to the default values.
```json
{
  color: "red",
  content: "*"
}
```

### To do

Refer to the issues of this repository.

